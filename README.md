# ansible-role-dns
Ansible role to manage resolv.conf

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://github.com/adieperi/ansible-role-resolv.git
  scm: git
  version: master
  name: ansible-role-resolv
```
### Download the role
```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```

## Requirements

- Ansible >= 2.9 **(No tests has been realized before this version)**

## Role Variables

All variables which can be overridden are stored in [default/main.yml](default/main.yml) file as well as in table below.

| Name           | Default Value | Choices | Description                        |
| -------------- | ------------- | ------- | -----------------------------------|
| `resolv_nameservers` | "1.0.0.2 - 1.1.1.2" | A nameservers list | A list of up to 3 nameserver IP addresses. |
| `resolv_domain` | "[]" | A local domain name | A local domain name. |
| `resolv_search` | "[]" | A domains names list | A list of up to 6 domains to search for host-name lookup. |
| `resolv_sortlist` | "[]" | An IP/netmask addresses list | This option allows addresses returned by [gethostbyname](https://linux.die.net/man/3/gethostbyname) to be sorted. |
| `resolv_options` | "[]" | An options list | Options allows certain internal resolver variables to be modified. |
| `resolv_set_immutable` | "true" | true / false | Used to define if the file will be immutable. |

**PS :** I recommend you to read the user manual via [this link](https://linux.die.net/man/5/resolv.conf) or via the `man resolv.conf` command under Linux.

## Example Playbook
```yaml
---
- hosts: all
  tasks:
    - name: Include ansible-role-resolv
      include_role:
        name: ansible-role-resolv
      vars:
        resolv_nameservers:
          - 10.1.0.2
          - 190.12.4.67
          - 1.1.1.1
        resolv_search:
          - domain.net
        resolv_options:
          - rotate
        resolv_set_immutable: false
```
## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
